﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P01_Verastegui_Johann
{
    class Almacenaje_y_organización : Muebles
    {
        public Almacenaje_y_organización(string name) : base(name)
        {
        }

        public Almacenaje_y_organización(float precio) : base(precio)
        {
        }

        public Almacenaje_y_organización(int CanPatas) : base(CanPatas)
        {
        }
    }
}
