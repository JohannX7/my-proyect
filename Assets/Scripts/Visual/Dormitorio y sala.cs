﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P01_Verastegui_Johann
{
    class Dormitorio_y_sala : Muebles
    {
        public Dormitorio_y_sala(string name) : base(name)
        {
        }

        public Dormitorio_y_sala(float precio) : base(precio)
        {
        }

        public Dormitorio_y_sala(int CanPatas) : base(CanPatas)
        {
        }
    }
}
