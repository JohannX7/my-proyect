﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P01_Verastegui_Johann
{
    class Muebles
    {
        public string name;
        public float precio = 70;
        public int CanPatas = 4;

        public Muebles(string name)
        {
            this.name = name;
        }

        public Muebles(float precio)
        {
            this.precio = precio;
        }

        public Muebles(int CanPatas)
        {
            this.CanPatas = CanPatas;
        }
    }
}
